import { createReducer } from '../utils';
import * as constants from '../constants'

const collectionsInitState = [];
const collectionInitState = {};

export const collections = createReducer(collectionsInitState,{
    [constants.RECEIVE_COLLECTIONS]:(state, action)=>{
        return [...action.collections];
    }
});
export const collection = createReducer(collectionInitState,{
    [constants.RECEIVE_COLLECTION]:(state, action)=>{
        return action.collection;
    }
});