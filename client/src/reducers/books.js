import { createReducer } from '../utils';
import * as constants from '../constants'

const booksInitState = [];
const bookInitState = {};

export const books = createReducer(booksInitState,{
    [constants.RECEIVE_BOOKS]:(state,action)=>{
        return [...action.books];
    }
});
export const book = createReducer(bookInitState,{
    [constants.RECEIVE_BOOK]:(state,action)=>{
        return action.book;
    }
});