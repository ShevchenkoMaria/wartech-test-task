import { combineReducers } from 'redux';
import { books, book } from './books';
import {collections, collection } from './collections'

const rootReducer = combineReducers({
    collections,
    collection,
    books,
    book
});

export default rootReducer;