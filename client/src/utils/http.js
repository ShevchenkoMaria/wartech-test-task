import axios from "axios";

class Http {
    static _axios(config){
        if(!config || !config.url){
            throw {
                message: "Http request error: config required"
            };
        }

        config.method = config.method || 'get';

        if(config.url && config.url.indexOf('http') === -1 && config.url.indexOf('https') === -1 && config.url[0] !== '/')
            config.url = '/' + config.url

        return axios(config).then((response)=>{
            //console.log("HTTP response received: ",response);
            return response.data;
        }).catch((error)=>{
            console.error("HTTP request error: ", error);
            return {};
        })
    }
}


export default Http;