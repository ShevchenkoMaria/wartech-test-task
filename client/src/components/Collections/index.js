import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { createCollection } from '../../actions';

class CollectionsPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            collections: this.props.collections,
            showCreate: false,
            newName: '',
            newDescription: ''
        };
    }
    componentWillMount() {}
    componentWillReceiveProps(nextProps) {
        if (nextProps.collections !== this.props.collections) {
            this.setState({collections: nextProps.collections})
        }
    }

    _onCreateNewCollection(){
        let data = {
            name: this.state.newName,
            description: this.state.newDescription
        };
        this.props.createCollection(data);
        this.setState({showCreate: false})
    }
    _onToggleCreate(){
        this.setState({showCreate: !this.state.showCreate})
    }

    _onEditName(event){
        this.setState({newName: event.target.value});
    }

    _onEditDescription(event){
        this.setState({newDescription: event.target.value});
    }
    _onCancelCreate(){
        this.setState({
            newDescription: '',
            newName: '',
            showCreate: false
        });
    }
    renderCollections(){
        let collections =  this.state.collections.map((collection)=>
                <div key={collection._id} className="row padding-5" to={`/collections/${collection._id}`}>
                    <div  className="col-xs-4">
                        <Link to={`/collections/${collection._id}`}>
                            <p>{collection.name}</p>
                        </Link>
                    </div>
                    <div  className="col-xs-8"><p>{collection.description}</p></div>
                </div>
        );
        return <div>{collections}</div>
    }

    renderCreateNew(){
        if(this.state.showCreate){
            return <div className="popup create-popup">
                <div className="container-fluid">
                    <div className="row padding-5">
                        <div className="col-xs-4"><h3>Name</h3></div>
                        <div  className="col-xs-8">
                            <input className="form-control" value={this.state.newName} onChange={this._onEditName.bind(this)} />
                        </div>

                    </div>
                    <div className="row padding-5">
                        <div className="col-xs-4"><h3>Description</h3></div>
                        <div  className="col-xs-8">
                            <input className="form-control" value={this.state.newDescription} onChange={this._onEditDescription.bind(this)} />
                        </div>
                    </div>
                    <div className="row padding-5">
                        <button type="button" className="btn btn-primary" onClick={this._onCreateNewCollection.bind(this)}>Create</button>
                        <button type="button" className="btn btn-default" onClick={this._onCancelCreate.bind(this)}>Cancel</button>
                    </div>
                </div>
            </div>
        }
    }
    render(){
        return (
            <div>
                <div className='container-fluid'>
                    <div className="row padding-5 relative">
                        <h2 className="left">Collections</h2>
                        <button type="button" className="btn btn-primary right" onClick={this._onToggleCreate.bind(this)}>Create New</button>
                        {this.renderCreateNew()}
                    </div>
                    <div className="row padding-5">
                        <div className="col-xs-4"><h3>Name</h3></div>
                        <div className="col-xs-8"><h3>Description</h3></div>
                    </div>
                    {this.renderCollections()}
                </div>
            </div>
        );
    }
}
CollectionsPage.propTypes = {
    createCollection: PropTypes.func.isRequired,
    collections: PropTypes.array,
    collection: PropTypes.object
};

const mapStateToProps = (state)=>{
    return {
        collections: state.collections,
        collection: state.collection
    };
};

export default connect(mapStateToProps, {createCollection})(CollectionsPage);