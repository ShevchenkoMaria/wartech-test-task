import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router'
import {getCollection, removeBook, editCollection, addBook, deleteCollection} from '../../actions'

class CollectionDetails extends Component {
    constructor(props){
        super(props);
        this.state = {
            collection: props.collection,
            books: props.books,
            editCollection: false,
            newName: props.collection.name,
            newDescription: props.collection.description,
            showAddBooksBlock: false
        };
    }
    componentWillMount() {
        this.props.getCollection(this.props.collection_id);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.collection !== this.props.collection) {
            if(!nextProps.collection){
                browserHistory['replace']('/collections');
            }
            else
                this.setState({
                    collection: nextProps.collection,
                    newName: nextProps.collection.name,
                    newDescription: nextProps.collection.description
                })
        }
        if (nextProps.books !== this.props.books) {
            this.setState({books: nextProps.books})
        }
    }


    _onDeleteBook(bookId){
        this.props.removeBook(this.state.collection._id, bookId);
    }

    _onToggleEditCollection(){
        this.setState({editCollection: !this.state.editCollection});
    }

    _onToggleEditRating(){
        this.setState({ediRating: !this.state.ediRating});
    }

    _onToggleShowAddBooksBlock(){
        this.setState({showAddBooksBlock: !this.state.showAddBooksBlock});
    }

    _onEditName(event){
        this.setState({newName: event.target.value});
    }

    _onEditDescription(event){
        this.setState({newDescription: event.target.value});
    }

    _onSaveEditCollection(){
        let data = {
            name: this.state.newName,
            description: this.state.newDescription
        };
        this.props.editCollection(this.state.collection._id, data);
        this.setState({
            editCollection: false
        });
    }

    _onCancelEdit(){
        this.setState({
            newDescription: this.state.collection.description,
            newName: this.state.collection.name,
            editCollection: false
        });
    }

    _onAddBook(bookId){
        let data = {
            bookId: bookId
        };
        this.props.addBook(this.state.collection._id, data);
    }
    _onDeleteCollection(){
        this.props.deleteCollection(this.state.collection._id);
    }

    renderBooks(){
        let collections = this.state.collection.books.map(book =>
            <div key={book._id} className="row padding-5">
                <div className="col-xs-2">{book.name}</div>
                <div className="col-xs-2">{book.author}</div>
                <div className="col-xs-2">{book.price}</div>
                <div className="col-xs-2">{book.rating}</div>
                <div className="col-xs-2 clickable" onClick={this._onDeleteBook.bind(this, book._id)}>Remove</div>
            </div>
        );
        return <div className="row padding-5">{collections}</div>
    }

    renderAddBooksBlock(){
        let rendersBooks = this.state.books.filter(book => {
            let books = this.state.collection.books.filter(collectionBook=>{
                return collectionBook._id === book._id
            });
            return books.length === 0;
        });
        if(this.state.showAddBooksBlock){
            let books = rendersBooks.map(book =>
                <div key={book._id} className="list-item" onClick={this._onAddBook.bind(this, book._id)}>{book.name}</div>
            );
            return <div className="popup">{books}</div>
        }
    }
    renderNameCollection(){
        if(this.state.editCollection)
            return <div  className="col-xs-8">
                    <input className="form-control" value={this.state.newName} onChange={this._onEditName.bind(this)} />
                </div>
        else
            return <div className="col-xs-8">{this.state.collection.name}</div>
    }
    renderDescriptionCollection(){
        if(this.state.editCollection)
            return <div  className="col-xs-8">
                    <textarea className="form-control" value={this.state.newDescription} onChange={this._onEditDescription.bind(this)} />
                </div>
        else
            return <div className="col-xs-8">{this.state.collection.description}</div>
    }
    renderButtons(){
        if(this.state.editCollection){
            return <div>
                <button type="button" className="btn btn-primary" onClick={this._onSaveEditCollection.bind(this)}>Save</button>
                <button type="button" className="btn btn-default" onClick={this._onCancelEdit.bind(this)}>Cancel</button>
            </div>
        }
        else
            return <div>
                <button type="button" className="btn btn-primary" onClick={this._onToggleEditCollection.bind(this)}>Edit</button>
                <button type="button" className="btn btn-primary" onClick={this._onDeleteCollection.bind(this)}>Delete</button>
            </div>
    }
    renderLayout(){
        return (
            <div className="container">
                <div className="row padding-5">
                    <h2 className="left">Collection</h2>
                    <div className="right col-xs-2">{this.renderButtons()}</div>
                </div>
                <div className="row padding-5">
                    <div className="col-xs-2"><h3>Name</h3></div>
                    {this.renderNameCollection()}

                </div>
                <div  className="row padding-5">
                    <div className="col-xs-2"><h3>Description</h3></div>
                    {this.renderDescriptionCollection()}
                </div>

                <div className="row padding-5">
                    <div className="col-xs-2"><h3>Books</h3></div>
                    <div className="col-xs-10">
                        <div className="container-fluid">
                            <div className="row padding-5">
                                <div className="col-xs-2"><h4>name</h4></div>
                                <div className="col-xs-2"><h4>author</h4></div>
                                <div className="col-xs-2"><h4>price</h4></div>
                                <div className="col-xs-2"><h4>rating</h4></div>
                                <div className="col-xs-2 clickable" onClick={this._onToggleShowAddBooksBlock.bind(this)}>
                                    <h4>Add</h4>
                                    {this.renderAddBooksBlock()}
                                </div>
                            </div>
                            {this.renderBooks()}
                        </div>
                    </div>
                </div>

            </div>
        );
    }

    render(){
        if(this.state.collection._id){
            return this.renderLayout()
        }
        else return <h3>Loading...</h3>
    }
}
CollectionDetails.propTypes = {
    collection: PropTypes.object,
    collection_id: PropTypes.string,
    books: PropTypes.array
};

const mapStateToProps = (state, ownProps)=>{
    let collection_id = ownProps.params.collection_id;
    let collection = state.collection || null;
    if(collection._id){
        // I do this, because when I get collection by id and
        // when I get collection after edit, collection.books has different data form
        if(collection.books.length && !collection.books[0]._id){
            collection.books = state.books.filter(book => {
                let books = collection.books.filter(bookId => {
                    return book._id === bookId
                });
                return books.length > 0;
            })
        }
    }

    return {
        collection: collection,
        collection_id: collection_id,
        books: state.books
    };
};

export default connect(mapStateToProps, {getCollection, removeBook, editCollection, addBook, deleteCollection})(CollectionDetails);