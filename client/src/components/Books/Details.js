import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router'
import { editBook, getBook, deleteBook } from '../../actions'

class BookDetails extends Component {
    constructor(props){
        super(props);
        this.state = {
            book: props.book,
            newName: '',
            newAuthor: '',
            newPrice: 0,
            newRating: 0,
            editRating: false,
            editBook: false
        };
    }
    componentWillMount() {
        this.props.getBook(this.props.book_id);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.book !== this.props.book) {
            if(!nextProps.book){
                browserHistory['replace']('/books');
            }
            else
                this.setState({
                    book: nextProps.book,
                    newAuthor: nextProps.book.author,
                    newName: nextProps.book.name,
                    newPrice: nextProps.book.price,
                    newRating: nextProps.book.rating
                })
        }
    }
    _onToggleEditRating(){
        this.setState({editRating: !this.state.ediRating});
    }
    _onToggleEditBook(){
        this.setState({editBook: !this.state.ediRating});
    }

    _onDeleteBook(){
        this.props.deleteBook(this.state.book._id);
    }

    _onRate(){
        let data = {
            rating: this.state.newRating
        };
        this.props.editBook(this.state.book._id, data);
        this.setState({editRating: false})
    }
    _onSaveEditBook(){
        let data = {
            name: this.state.newName,
            author: this.state.newAuthor,
            price: this.state.newPrice
        };
        this.props.editBook(this.state.book._id, data);
        this.setState({editBook: false})
    }
    _onEditName(event){
        this.setState({newName: event.target.value});
    }

    _onEditAuthor(event){
        this.setState({newAuthor: event.target.value});
    }
    _onEditPrice(event){
        if(event.target.value >=0)
            this.setState({newPrice: event.target.value});
    }

    _onEditRating(event){
        if(event.target.value >=0 && event.target.value <= 5){
            this.setState({newRating: event.target.value});
        }
    }
    _onCancelEdit(){
        this.setState({
            newAuthor: this.state.book.author,
            newName: this.state.book.name,
            newPrice: this.state.book.price,
            editBook: false
        });
    }
    _onCancelRating(){
        this.setState({
            newRating: this.state.book.rating,
            editRating: false
        });
    }

    _onToggleCreate(){
        this.setState({showCreate: !this.state.showCreate})
    }


    renderNameBook(){
        if(this.state.editBook)
            return <div  className="col-xs-8">
                <input className="form-control" value={this.state.newName} onChange={this._onEditName.bind(this)} />
            </div>
        else
            return <div className="col-xs-8">{this.state.book.name}</div>
    }
    renderAuthorBook(){
        if(this.state.editBook)
            return <div  className="col-xs-8">
                <input className="form-control" value={this.state.newAuthor} onChange={this._onEditAuthor.bind(this)} />
            </div>
        else
            return <div className="col-xs-8">{this.state.book.author}</div>
    }
    renderPriceBook(){
        if(this.state.editBook)
            return <div  className="col-xs-8">
                <input className="form-control" value={this.state.newPrice} onChange={this._onEditPrice.bind(this)} />
            </div>
        else
            return <div className="col-xs-8">{this.state.book.price}</div>
    }
    renderRatingBook(){
        if(this.state.editRating)
            return <div  className="col-xs-8">
                <input className="form-control" value={this.state.newRating} onChange={this._onEditRating.bind(this)} />
            </div>
        else
            return <div className="col-xs-8">{this.state.book.rating}</div>
    }

    renderRateButtons(){
        if(this.state.editRating)
            return <div>
                <button type="button" className="btn btn-primary" onClick={this._onRate.bind(this)}>Rate</button>
                <button type="button" className="btn btn-default" onClick={this._onCancelRating.bind(this)}>Cancel</button>
            </div>
        else
            return <div>
                <button type="button" className="btn btn-primary" onClick={this._onToggleEditRating.bind(this)}>Rate</button>
            </div>
    }
    renderButtons(){
        if(this.state.editBook){
            return <div>
                <button type="button" className="btn btn-primary" onClick={this._onSaveEditBook.bind(this)}>Save</button>
                <button type="button" className="btn btn-default" onClick={this._onCancelEdit.bind(this)}>Cancel</button>
            </div>
        }
        else
            return <div>
                <button type="button" className="btn btn-primary" onClick={this._onToggleEditBook.bind(this)}>Edit</button>
                <button type="button" className="btn btn-primary" onClick={this._onDeleteBook.bind(this)}>Delete</button>
            </div>
    }
    renderLayout(){
        return (
            <div className="container">
                <div className="row padding-5">
                    <h2 className="left">Book</h2>
                    <div className="right col-xs-2">{this.renderButtons()}</div>
                </div>
                <div className="row padding-5">
                    <div className="col-xs-2"><h3>Name</h3></div>
                    {this.renderNameBook()}
                </div>
                <div  className="row padding-5">
                    <div className="col-xs-2"><h3>Author</h3></div>
                    {this.renderAuthorBook()}
                </div>
                <div  className="row padding-5">
                    <div className="col-xs-2"><h3>Price</h3></div>
                    {this.renderPriceBook()}
                </div>
                <div  className="row padding-5">
                    <div className="col-xs-2"><h3>Rating</h3></div>
                    {this.renderRatingBook()}
                    <div className="col-xs-2">{this.renderRateButtons()}</div>
                </div>
            </div>
        );
    }

    render(){
        if(this.state.book && this.state.book._id){
            return this.renderLayout()
        }
        else return <h3>Loading...</h3>
    }
}
BookDetails.propTypes = {
    book: PropTypes.object,
    book_id: PropTypes.string
};

const mapStateToProps = (state, ownProps)=>{
    let book_id = ownProps.params.book_id;
    let book = state.book || null;
    return {
        book: book,
        book_id: book_id
    };
};

export default connect(mapStateToProps, {editBook, getBook, deleteBook})(BookDetails);