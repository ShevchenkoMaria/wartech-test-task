import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux'
import { Router, browserHistory } from 'react-router';
import './index.css';
import configureStore from './store/configureStore';
import routes from './routes';
import registerServiceWorker from './registerServiceWorker';

const store = configureStore;

render(
    <Provider store={store}>
        <div>
            <Router history={browserHistory} routes={routes(store)} />
        </div>
    </Provider>,
    document.getElementById('root')
)
registerServiceWorker();