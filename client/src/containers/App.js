import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Collections from '../components/Collections';
import Books from '../components/Books';

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            showBooks: false
        };
    }
    componentWillMount() {
    }
    componentWillReceiveProps(nextProps) {
    }
    renderPage(){
        if(this.state.showBooks){
            return <Books />
        }
        else return <Collections />
    }

    _onOpenBooks(){
        this.setState({showBooks: true})
    }
    _onOpenCollections(){
        this.setState({showBooks: false})
    }
    render(){
        return (
            <div  className="container">
                <div className="tabs">
                    <Link to={`/books`}><h3 className="tab">Books</h3></Link>
                    <Link to={`/collections`}><h3 className="tab">Collections</h3></Link>
                </div>
                <div>{this.props.children}</div>
            </div>
        );
    }
}
App.propTypes = {
    children: PropTypes.object
};

const mapStateToProps = (state)=>{
    return {
    };
};

export default connect(mapStateToProps, {})(App);